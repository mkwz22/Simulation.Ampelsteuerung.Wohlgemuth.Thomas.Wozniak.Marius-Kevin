CONTRIBUTING
============

## Developer

* Thomas Wohlgemuth 
    *Support / Design / Code*
    
* Marius-Kevin Wozniak
    *Design / Code / Documentation*

## Contact

Thomas.Wohlgemuth@iem.thm.de
marius-kevin.wozniak@iem.thm.de