A simulation of a traficlight
=============================

## Introduction
The project is a simulation of a traficlight at a crossroad.
It is connected with Board of Symbols to display the status of the lights.

## What does it do?
* sends the status of the lights to Board of Symbols
* simulates the status of the signal lights
* different lighting simulation

## About
Version 00.00
www.apache.org/licences/LICENCE-2.0
*coded by a german car driver*